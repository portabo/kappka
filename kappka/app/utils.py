from django.conf import settings
from influxdb_client import InfluxDBClient


def get_influxdb_client():
    return InfluxDBClient(
        url=f"http://{settings.INFLUXDB_HOST}:{settings.INFLUXDB_PORT}",
        token=settings.INFLUXDB_TOKEN,
        org=settings.INFLUXDB_ORG,
    )
