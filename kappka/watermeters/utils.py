import plotly.express as px
import folium
from .models import Watermeter
from django.contrib.humanize.templatetags.humanize import naturaltime, intcomma


def get_daily_graph(df):
    y_title = "Hodinová spotřeba v litrech"
    x_title = "Čas v hodinách"

    config = {"displaylogo": False}

    df["ticks"] = df["datetime"]

    try:
        fig = px.line(df, x="ticks", y="value", color_discrete_sequence=["#1ba2e6"])
        fig.update_layout(
            yaxis_title=y_title,
            xaxis_title=x_title,
            paper_bgcolor="#FFFFFF",
            plot_bgcolor="#FFFFFF",
            font=dict(family="Futura", size=16, color="#000000"),
            hovermode="x unified",
            hoverlabel=dict(bgcolor="white", font_size=16, font_family="Futura"),
            separators=",",
        )
        fig.update_traces(mode="markers+lines", hovertemplate="%{y} l/hod")
        fig.update_xaxes(
            title_font_size=16,
            showline=True,
            mirror=False,
            linewidth=2,
            linecolor="black",
            showgrid=True,
            tickformat="%H:%M",
            rangemode="tozero",
            fixedrange=True,
        )
        fig.update_yaxes(
            title_font_size=16,
            showline=True,
            mirror=False,
            linewidth=2,
            linecolor="black",
            rangemode="tozero",
            showgrid=True,
            gridwidth=1,
            gridcolor="#6c757d",
            fixedrange=True,
        )
        fig = fig.to_html(config=config)
    except Exception as e:
        print("Problém při tvorbě grafu.")
        print(e)
        fig = None

    return fig


def get_weekly_graph(df):
    # TODO: Přidat mezeru pro tisícovky.
    y_title = "Denní spotřeba v litrech"
    x_title = None
    config = {"displaylogo": False}

    df["ticks"] = (
        df["datetime"].dt.day_name(locale="cs_CZ.UTF-8")
        + " ("
        + df["datetime"].dt.strftime("%d.%m.")
        + ")"
    )

    try:
        fig = px.bar(df, x="ticks", y="value", color_discrete_sequence=["#1ba2e6"])
        fig.update_layout(
            yaxis_title=y_title,
            xaxis_title=x_title,
            paper_bgcolor="#FFFFFF",
            plot_bgcolor="#FFFFFF",
            font=dict(family="Futura", size=16, color="#000000"),
            hoverlabel=dict(bgcolor="white", font_size=16, font_family="Futura"),
            separators=",",
        )
        fig.update_traces(
            hovertemplate="%{y} l/den",
        )
        fig.update_xaxes(
            title_font_size=16,
            showline=True,
            mirror=False,
            linewidth=2,
            linecolor="black",
            showgrid=True,
            rangemode="tozero",
            fixedrange=True,
        )
        fig.update_yaxes(
            title_font_size=16,
            showline=True,
            mirror=False,
            linewidth=2,
            linecolor="black",
            rangemode="tozero",
            showgrid=True,
            gridwidth=1,
            gridcolor="#6c757d",
            fixedrange=True,
        )
        fig = fig.to_html(config=config)
    except Exception as e:
        print("Problém při tvorbě grafu.")
        print(e)
        fig = None

    return fig


def get_map(objs, detail=False):
    coordinates = (50.772556, 14.212761)
    # coordinates = [(obj.latitude, obj.longitude) for obj in objs] TODO: Připraveno pro výpočet centroidu, automatické uchycení v mapě, není třeba uvádět natvrdo GPS souřadnice.

    map = folium.Map(
        location=coordinates, zoom_start=12
    )  # TODO: Změnit lokaci na základě objektů.

    for obj in objs:
        try:
            obj_coordinates = (obj.latitude, obj.longitude)

            if detail:
                popup = folium.Popup(
                    f'<div class="fs-4">Souřadnice: <a class="text-nowrap fs-4" href="https://www.google.com/maps/place/{obj_coordinates[0]}%20{obj_coordinates[1]}" target="_top">{obj_coordinates[0]}, {obj_coordinates[1]}</a></div>',
                )
            else:
                popup = folium.Popup(
                    f"""
                    <div class="fs-5 text-nowrap">
                        <h4 class="">{obj.name if obj.name else obj.id}</h4>
                        <ul class="list-unstyled">
                            <li><span class="fw-semibold">Stav vodoměru:</span> {intcomma(obj.last_record_value) if obj.last_record_value else "–" } l</li>
                            <li><span class="fw-semibold">Denní spotřeba:</span> {intcomma(obj.daily_consumption) if obj.daily_consumption else "–" } l</li>
                            <li><span class="fw-semibold">Poslední aktualizace:</span> {naturaltime(obj.last_record_time) if obj.last_record_time else "–" }</li>
                        </ul>
                        <a href="/vodomery/{obj.id}" target="_top">Detailní přehled</a>
                    </div>
                    """,
                )
            folium.Marker(location=obj_coordinates, popup=popup).add_to(
                map
            )  # TODO: Přepsat tak, aby se vygeneroval geojson. Je nutné kompletně připravit tooltip.
        except Exception as e:
            print("Problém při přidání objektu.")
            print(e)

    try:
        map = map._repr_html_()
    except:
        map = None

    return map


# Toto je filtrování v rámci view, mělo by však jít implementovat jako templatetags
def filter_data(var, filter_list):
    if var.status in filter_list:
        return True
    else:
        return False


# Toto je řazení v rámci view, mělo by však jít implementovat jako templatetags, případně v rámci Jinja pomocí dictsort.
def sort_data(objs, sort):
    match sort:
        case Watermeter.Sort.CONSUMPTION_HOURLY:
            return sorted(
                objs,
                key=lambda ws: (
                    ws.hourly_consumption is not None,
                    ws.hourly_consumption,
                ),
                reverse=True,
            )
        case Watermeter.Sort.CONSUMPTION_DAILY:
            return sorted(
                objs,
                key=lambda ws: (
                    ws.daily_consumption is not None,
                    ws.daily_consumption,
                ),
                reverse=True,
            )
        case Watermeter.Sort.CONSUMPTION_TOTAL:
            return sorted(
                objs,
                key=lambda ws: (
                    ws.last_record_value is not None,
                    ws.last_record_value,
                ),
                reverse=True,
            )
        case default:
            return sorted(objs, key=lambda ws: ws.id)


def get_view_template(view_type):
    match view_type:
        case Watermeter.ViewType.TABLE:
            return "watermeters/partials/view/table.html"
        case Watermeter.ViewType.MAP:
            return "watermeters/partials/view/map.html"
        case default:
            return "watermeters/partials/view/cards.html"
