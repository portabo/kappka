from django.db import models
from django.urls import reverse
from app.utils import get_influxdb_client
from django.conf import settings
from django.db.models import Q
from datetime import datetime, timezone, timedelta
import os


class WatermeterQuerySet(models.QuerySet):
    def search(self, query=None):
        if query is None or query == "":
            return self.none()
        lookups = (
            Q(id__icontains=query)
            | Q(name__icontains=query)
            | Q(address__icontains=query)
            | Q(district__icontains=query)
            | Q(post_code__icontains=query)
        )
        return self.filter(lookups)


class WatermeterManager(models.Manager):
    def get_queryset(self):
        return WatermeterQuerySet(self.model, using=self._db)

    def search(self, query=None):
        return self.get_queryset().search(query=query)


class Watermeter(models.Model):
    # TODO: Zkontrolovat model, zejména věci do databáze, zda jsou blank/null apod.
    class Status(models.TextChoices):
        ON = "1", "Aktivní"
        DELAYED = "d", "Zpožděné"
        OFF = "0", "Neaktivní"
        UNKNOWN = "u", "Neznámé"

    class Sort(models.TextChoices):
        ALPHABET = "a", "Abecedy"
        CONSUMPTION_TOTAL = "t", "Stavu"
        CONSUMPTION_DAILY = "d", "Denní spotřeby"
        CONSUMPTION_HOURLY = "h", "Aktuální spotřeby"

    class ViewType(models.TextChoices):
        CARDS = "c", "Karty"
        TABLE = "t", "Tabulka"
        MAP = "m", "Mapa"

    id = models.CharField(max_length=32, primary_key=True)
    name = models.CharField(max_length=128, null=True)
    description = models.CharField(
        max_length=256, null=True
    )  # TODO: Změnit na textové pole - něco většího.
    address = models.CharField(max_length=256, blank=True, null=True)
    latitude = models.FloatField(
        blank=True, null=True
    )  # TODO: Vytvořit validaci, případně změnit implementaci pomocí GeoDjango
    longitude = models.FloatField(
        blank=True, null=True
    )  # TODO: Vytvořit validaci, případně změnit implementaci pomocí GeoDjango
    district = models.CharField(max_length=32, blank=True, null=True)
    post_code = models.SmallIntegerField(
        blank=True, null=True
    )  # TODO: Vytvořit validaci

    objects = WatermeterManager()

    @property
    def alias(self):
        return self.name

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse("vodomery:detail", kwargs={"id": self.id})

    @property
    def generate_stats(self):
        # TODO: Přepsat chytřeji, ale základ je v pořádku. Pravděpodobně by šlo, aby to byl jeden request pro všechny vodoměry, stálo by to za to?
        client = get_influxdb_client()
        bucket = settings.INFLUXDB_BUCKET
        query_api = client.query_api()
        status = Watermeter.Status.UNKNOWN

        last_hour = query_api.query(
            f"""
                from(bucket: "{bucket}")
                    |> range(start: -1h)
                    |> filter(fn: (r) => r["_measurement"] == "{os.environ.get("INFLUXDB_MEASUREMENT_NAME")}")
                    |> filter(fn: (r) => r["place"] == "{self.id}")
                    |> group()
                    |> first()
                    |> yield(name: "last_hour")
            """
        )

        today = query_api.query(
            f"""
                from(bucket: "{bucket}")
                    |> range(start: today())
                    |> filter(fn: (r) => r["_measurement"] == "{os.environ.get("INFLUXDB_MEASUREMENT_NAME")}")
                    |> filter(fn: (r) => r["place"] == "{self.id}")
                    |> group()
                    |> first()
                    |> yield(name: "daily_consumption")
            """
        )

        last = query_api.query(
            f"""
                from(bucket: "{bucket}")
                    |> range(start: 0)
                    |> filter(fn: (r) => r["_measurement"] == "{os.environ.get("INFLUXDB_MEASUREMENT_NAME")}")
                    |> filter(fn: (r) => r["place"] == "{self.id}")
                    |> group()
                    |> last()
                    |> yield(name: "last_record")
            """
        )

        try:
            today_record_value = today[0].records[0]["_value"]
        except:
            print(
                f"{self.id} | Data nebyla stažena. Problém s připojením do Influx databáze / zpracováním dat z databáze (POSLEDNÍ DEN)."
            )
            today_record_value = None

        try:
            last_record = last[0].records[0]
            last_record_value = last_record["_value"]
            last_record_time = last_record["_time"]
        except:
            print(
                f"{self.id} | Data nebyla stažena. Problém s připojením do Influx databáze / zpracováním dat z databáze (POSLEDNÍ ZÁZNAM)."
            )
            last_record = None
            last_record_value = None
            last_record_time = None

        try:
            last_hour_record_value = last_hour[0].records[0]["_value"]
        except:
            print(
                f"{self.id} | Data nebyla stažena. Problém s připojením do Influx databáze / zpracováním dat z databáze (POSLEDNÍ HODINA)."
            )

            last_hour_record_value = None

        if last_record_value and last_hour_record_value:
            hourly_consumption = last_record_value - last_hour_record_value
        else:
            hourly_consumption = None

        if last_record_value and today_record_value:
            daily_consumption = last_record_value - today_record_value
        else:
            daily_consumption = None

        utc_time_now = datetime.now(timezone.utc) - timedelta(hours=1)
        if last_record_time > utc_time_now:
            status = Watermeter.Status.ON
        else:
            status = Watermeter.Status.DELAYED

        client.__del__()

        self.hourly_consumption = hourly_consumption
        self.last_record_value = last_record_value
        self.last_record_time = last_record_time
        self.daily_consumption = daily_consumption
        self.status = status

    def generate_data(self, period=None, aggregator=None):
        client = get_influxdb_client()
        bucket = settings.INFLUXDB_BUCKET
        query_api = client.query_api()

        if not period:
            period = "-25h"
            aggregator = "1h"

        try:
            df = query_api.query_data_frame(
                f"""
                    from(bucket: "{bucket}")
                            |> range(start: {period})
                            |> filter(fn: (r) => r["_measurement"] == "{os.environ.get("INFLUXDB_MEASUREMENT_NAME")}")
                            |> filter(fn: (r) => r["place"] == "{self.id}")
                            |> group()
                            |> aggregateWindow(every: {aggregator}, fn: last, createEmpty: true)
                            |> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")
                """
            )
            df.rename(columns={"_time": "datetime"}, inplace=True)
            df["datetime"] = (
                df["datetime"].dt.tz_convert("Europe/Prague").dt.tz_localize(None)
            )
            df.set_index("datetime", inplace=True)
            df = df[["value"]].diff()
            df.reset_index(inplace=True)
            df["datetime"] = df["datetime"].shift(1)
            df = df.iloc[1:]
        except:
            print(
                "Data nebyla stažena. Problém s připojením do Influx databáze / zpracováním dat z databáze."
            )
            df = None

        client.__del__()

        return df
