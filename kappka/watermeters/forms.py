from django import forms
from .models import Watermeter

hx_include = "[name='f'], [name='q'], [name='s'], [name='v']"
url = "/vodomery/"


class SearchForm(forms.Form):
    q = forms.CharField(
        required=False,
        label=False,
        widget=forms.TextInput(
            attrs={
                "class": "form-control border border-2 border-secondary rounded-5 fw-semibold text-secondary",
                "placeholder": "Název, adresa...",
                "hx-get": url,
                "hx-target": "#base-view",
                "hx-trigger": "keyup changed delay:400ms",
                "hx-include": hx_include,
            },
        ),
    )


class FilterSortForm(forms.Form):
    f = forms.MultipleChoiceField(
        label=False,
        widget=forms.CheckboxSelectMultiple(
            attrs={
                "hx-get": url,
                "hx-target": "#base-view",
                "hx-trigger": "change",
                "hx-include": hx_include,
            }
        ),
        choices=Watermeter.Status.choices,
        initial=Watermeter.Status.values,
    )

    s = forms.ChoiceField(
        label=False,
        widget=forms.RadioSelect(
            attrs={
                "hx-get": url,
                "hx-target": "#base-view",
                "hx-trigger": "change",
                "hx-include": hx_include,
            }
        ),
        choices=Watermeter.Sort.choices,
        initial=Watermeter.Sort.ALPHABET,
    )


class ViewTypeForm(forms.Form):
    v = forms.ChoiceField(
        label=False,
        widget=forms.RadioSelect(
            attrs={
                "class": "btn-check",
                "hx-get": url,
                "hx-target": "#base-view",
                "hx-trigger": "change",
                "hx-include": hx_include,
            }
        ),
        choices=Watermeter.ViewType.choices,
        initial=Watermeter.ViewType.CARDS,
    )
