from django.urls import path
from .views import (
    watermeter_main_view,
    watermeter_detail_view,
    watermeter_detail_hx_view,
)

app_name = "vodomery"
urlpatterns = [
    path("", watermeter_main_view, name="base"),
    path("hx/<str:id>/", watermeter_detail_hx_view, name="hx-detail"),
    path("<str:id>/", watermeter_detail_view, name="detail"),
]
