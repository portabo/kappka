from django import template
from ..models import Watermeter
from django.templatetags.static import static

register = template.Library()


@register.filter
def get_view_icon(value):
    return static(f"vendors/images/icons/{Watermeter.ViewType(value).name.lower()}.svg")
