from django.shortcuts import render
from django.http import HttpResponse, Http404
from django.urls import reverse
from .models import Watermeter
from .utils import (
    get_daily_graph,
    get_weekly_graph,
    get_map,
    filter_data,
    sort_data,
    get_view_template,
)
from .forms import SearchForm, FilterSortForm, ViewTypeForm


def watermeter_detail_view(request, id=None):
    hx_url = reverse("vodomery:hx-detail", kwargs={"id": id})

    # TODO: Title by šel pravděpodobně lepším způsobem než takto.
    try:
        obj = Watermeter.objects.get(id=id)
        title = obj.name if obj.name else obj.id
    except Exception as e:
        print(e)

    context = {"hx_url": hx_url, "title": title}
    return render(request, "watermeters/hx-detail-view.html", context)


def watermeter_detail_hx_view(request, id=None):
    if not request.htmx:
        raise Http404
    try:
        obj = Watermeter.objects.get(id=id)
        obj.generate_stats
        daily_data = obj.generate_data()
        weekly_data = obj.generate_data("-8d", "1d")

        daily_graph = get_daily_graph(daily_data)
        weekly_graph = get_weekly_graph(weekly_data)
        map = get_map([obj], detail=True)
    except Exception as e:
        print(e)
        obj = None
        daily_graph = None
        weekly_graph = None
        map = None

    if obj is None:
        return HttpResponse("Nenalezeno.")

    context = {
        "object": obj,
        "daily_graph": daily_graph,
        "weekly_graph": weekly_graph,
        "map": map,
    }

    return render(request, "watermeters/partials/detail/detail.html", context)


def watermeter_main_view(request):
    search_form = SearchForm(request.GET or None)
    filter_sort_form = FilterSortForm(request.GET or None)
    view_type_form = ViewTypeForm(request.GET or None)

    map = None
    view_type = None

    if search_form.is_valid() and search_form.cleaned_data["q"] != "":
        query = search_form.cleaned_data["q"]
        qs = Watermeter.objects.search(query=query)
    else:
        qs = Watermeter.objects.all()

    # TODO: Některé funkce by bylo vhodné naprogramovat jako v rámci custom managera, poté půjde volat "Watermeter.objects.stats"

    for ws in qs:
        ws.generate_stats

    if filter_sort_form.is_valid():
        filters = filter_sort_form.cleaned_data["f"]
        sort = filter_sort_form.cleaned_data["s"]

        qs = filter(lambda x: filter_data(x, filters), qs)
        qs = sort_data(qs, sort)

    if view_type_form.is_valid():
        view_type = view_type_form.cleaned_data["v"]

        if view_type == Watermeter.ViewType.MAP:
            map = get_map(qs)

    context = {
        "search_form": search_form,
        "filter_sort_form": filter_sort_form,
        "view_type_form": view_type_form,
        "queryset": qs,
        "show_filter": False,
        "map": map,
        "title": "Vodoměry",
    }

    template = get_view_template(view_type)

    # Kompletní logika vykreslování, a to jak v případě, že je request HMTX či nikoliv a zároveň, zda se jedná o první inicializaci (před vyhledáváním).
    if request.htmx:
        if not search_form.is_valid() and request.path != reverse(
            "vodomery:base"
        ):  # TODO: Tato podmínka asi není třeba, nikdy nenastane, bylo zde kvůli automatikcému refreshy, který je nefunkční.
            qs = qs[:5]
            qs = sort_data(qs, Watermeter.Sort.CONSUMPTION_HOURLY)
            context["queryset"] = qs
            return render(request, template, context)
        else:
            context["show_filter"] = True

            if view_type == Watermeter.ViewType.MAP:
                context["show_filter"] = "modify"

            response = render(request, template, context)
            response[
                "Cache-Control"
            ] = "no-store, max-age=0"  # Nutné, pokud používáme tlačítko "zpět" v prohlížeči, jinak HTMX nefunguje správně.
            return response
    else:
        if request.path == reverse("vodomery:base"):
            context["show_filter"] = True
            context["template"] = template
        else:
            qs = sort_data(qs, Watermeter.Sort.CONSUMPTION_HOURLY)
            qs = qs[:5]
            context["queryset"] = qs

    return render(request, "watermeters/base-view.html", context)
