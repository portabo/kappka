# Generated by Django 4.1.6 on 2023-04-02 20:20

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("watermeters", "0002_watermeter_full_name_alter_watermeter_name"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="watermeter",
            name="full_name",
        ),
        migrations.AddField(
            model_name="watermeter",
            name="description",
            field=models.CharField(max_length=256, null=True),
        ),
    ]
