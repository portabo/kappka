#!/bin/bash

npm install --prefix frontend
npm run --prefix frontend build
python manage.py collectstatic --noinput --clear
python manage.py makemigrations
python manage.py migrate
python manage.py loaddata watermeters
gunicorn app.wsgi:application --bind 0.0.0.0:8000