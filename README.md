# kAppka: Aplikace pro monitoring spotřeby vody

<img src="kappka/frontend/vendors/images/logos/kappka-logo.svg"
     alt="Logo aplikace kAppka"
     style="display: block; margin-left: auto; margin-right: auto; width: 256px;" />
<br>

kAppka je aplikace pro správu dat z chytrých vodoměrů, které měří spotřebu vody každou minutu a upozorňují na možné úniky vody nebo poruchy ve vodovodní instalaci. Aplikace vznikla ve spolupráci s městem Děčín, kde se osvědčila jako efektivní nástroj pro snižování nákladů na vodu a prevenci škod. Celý systém je naprogramován tak, aby jej mohl kdokoliv spustit a provozovat na vlastní platformě, více informací o tom, jak vlastní instanci aplikace zprovoznit naleznete níže v textu.

Aplikace kAppka běží na adrese [kappka.portabo.cz](https://kappka.portabo.cz/) jako součást datové platformy Portabo, kterou provozuje Datové centrum Ústeckého kraje, p. o. Provozovaná verze zpracovává informace o množství spotřebované vody na hlavních vodoměrech městských budov obce Děčín. Veškerá data z vodoměrů jsou ukládána do databáze, což umožňuje následně sledovat spotřebu v jednotlivých měsících či letech, nebo např. porovnat spotřebu vody mezi jednotlivými městskými budovami.

Aplikace je nadále vyvíjena a rozšiřována o další funkcionality, jako je např. možnost zasílat výstražné zprávy, nastavit cenové hladiny či exportovat historická data.

## Napsali o nás
- [Aplikace kAppka monitoruje spotřebu vody takřka z libovolného smart vodoměru](https://www.komunalniekologie.cz/info/aplikace-kappka-monitoruje-spotrebu-vody-takrka-z-libovolneho-smart-vodomeru)

# Hlavní funkce a popis implementace
Aplikace přijímá data z vodoměrů, která automaticky vyhodnocuje a vytváří z nich pohledy v podobě grafů a v případě překročení předpokládaných hodnot průtoků také automaticky posílá varovné zprávy kontaktním osobám.* Výhodou aplikace je, že není vázána na konkrétního výrobce vodovodních měřidel, díky čemuž lze vyčítat hodnoty prakticky z libovolného vodoměru (více informací o struktuře dat níže).

\* Některé funkcionality budou teprve implementovány, sledujte seznam plánovaných funkcí a vypublikované verze.

Aplikace je založená na open-source nástrojích, zejména aplikačním frameworku [Django](https://www.djangoproject.com/) (Python). Další použité knihovny jsou: [Bootstrap](https://getbootstrap.com/), [HTMX](https://htmx.org/) a [Webpack](https://webpack.js.org/). Popisná data vodoměrů jsou ukládána do [SQLite](https://sqlite.org/) databáze. Pro uložení zasílaných dat z vodoměrů je využívána TSDB databáze [InfluxDB](https://www.influxdata.com/) a nástroj sloužící pro sběr dat [Telegraf](https://www.influxdata.com/time-series-platform/telegraf/).

Aplikace je připravená tak, aby si ji mohl kdokoliv zprovoznit na vlastním serveru. Aplikace je připravena ve formě kontejnerů ([docker](https://www.docker.com/)), kterou lze jednoduše spustit na vlastní infrastruktuře pomocí [docker-compose](https://docs.docker.com/compose/).

## Struktura dat
Zasílat data do aplikace (databáze) lze aktuálně dvěma způsoby, a to buď formou HTTP POST, kdy Telegraf poslouchá na dané adrese (*TELEGRAF_HOST:TELEGRAF_PORT/TELEGRAF_PATH*, viz konfigurační soubor). Jakmile přijde zpráva, rozebere ji a uloží do databáze. Druhou možností, jak data ukládat je odposlech MQTT topicu (*MQTT_TOPIC*).

Zasílaná data musí být v následující JSON struktuře:
```
[
    {"misto": str, "stav": int},
    {"misto": str, "stav": int},
    ...
]
```

Příklad s reálnými daty níže. Pozn.: "misto" je ID vodoměru v rámci Django aplikace. Jak je vidět, jedná se o listu slovníků, kdy může být zasíláno až několik hodnot z vodoměrů najednou.
```
[
    {"misto": "zsvojanova", "stav": 57},
    {"misto": "msbela", "stav": 65},
    {"misto": "zsjaluvci", "stav": 6}
]
```

## Popis implementace

TO-DO...

# Jak začít?
Existuje několik možností, jak začít aplikaci provozovat na vlastní infrastruktuře. Celé řešení je připravené v rámci docker kontejnerů a připraveno pro spuštění pomocí docker-compose.

Existují tři profily v docker-compose:
1. **database** – Spustí databázi InfluxDB, včetně kolektoru dat (Telegraf).
2. **simulation** – Import dat do InfluxDB s tím, že časová značka bude odpovídat aktuánímu času - dva měsíce zpět. Následně začne simulace dat, která bude měsíc posílat data do Telegrafu (simulace realtime dat). Data odpovídají realitě, avšak je pouze změněné datum (čas v rámci dne odpovídá realitě).
3. **production** – Spuštění webové aplikace a NGINX serveru.

Profily lze kombinovat, tzn. pro spuštění produkční části: `docker-compose --profile production up`. V případě, že chcete spustit celé řešení: `docker-compose --profile database --profile simulation --profile production up`.

## Zdroj dat
Aplikaci můžete zprovoznit s testovými daty (simulace) či daty vlastními. Nutné je vytvořit konfigurační soubor → vytvořte kopii souboru *.env_example* a následně jej přejmenujte na *.env*.

Pro import popisných dat do SQLite databáze v rámci webové aplikace (Django), importujte popis vodoměrů tak, aby došlo ke "spárování dat", a to pomocí příkazu: `python manage.py loaddata watermeters` ze složky *kappka* (*kappka/fixtures/watermerters.json*). Tyto ukázková data lze nahradit svými, pozn.: **Párování dat InfluxDB a SQLite probíhá na základě proměnné "pk"**.

### Simulace dat
Pokud chcete data simulovat, spusťte profil `--profile simulation`.

### Vlastní data
Pokud chcete používat vlastní data, upravte soubor *watermeters.json* (*kappka/fixtures/watermerters.json*).

## Vývoj
Nutné je mít nainstalovaný python, pip, docker, docker-compose, npm (*node 18.15.0*).

Při vývoji zpravidla postupujeme tímto způsobem:
1. Spustíme vlastní instanci InfluxDB a Telegraf, včetně simulace dat: `docker-compose --profile database --profile simulation up` v rámci kontejnerového řešení.
2. Webovou aplikaci *Django* spouštíme mimo kontejner.
3. Dostaneme se do cesty s webovou aplikací (*kappka*).
4. Vytvoříme virtuální prostředí (Python): `python -m venv venv`.
5. Inicializujeme vytvořené virtuální prostředí: `source venv/bin/activate`.
6. Nainstalujeme potřebné balíčky: `pip install -r requirements.txt`.
7. Instalace potřebných npm balíčků: `npm install --prefix frontend`.
8. Spuštění prostředí pro auto-reload, generování *Sass* souborů apod.: `npm run --prefix frontend start`.
9. Připravení *SQLite* databáze: `python manage.py makemigrations` a `python manage.py migrate`.
10. Import popisných dat (viz soubor /kappka/fixtures/watermeters.json): `python manage.py loaddata watermeters`.
11. Spuštění *Django* aplikace: `python manage.py runserver`.

Nezapomeňte vytvořit a následně upravit *.env* soubor.

# Plánované funkce
Výčet funkcí, které plánujeme implementovat.

1. Notifikace – zasílání varovných zpráv (překročení definovaných hodnot průtoků, informace o výpadku služeb).
2. Cenové hladiny – orientační výpočet spotřeby v Kč.
3. Administrace a SSO – možnost přidání nového vodoměru a editace vodoměrů stávajících, možnost přidání notifikací, úprava cenových hladin.
4. Implementace nativní knihovny [Leaflet.js](https://leafletjs.com/) (mapa) – odproštění se od wrapperu Folium.
5. Cachování dat – využití cachování, zrychlení aplikace ("instantní" odezva).
6. Analýza a export dat – možnost procházet historické záznamy a export je do CSV.
7. Uživatelská nápověda – vysvětlení aplikace a prezentovaných proměnných.
8. SEO, sitemap.
9. SPA pomocí [Alpine.js](https://alpinejs.dev/) – přepsání dílčích komponent aplikace za účelem jejího zrychlení a zpracování dat zejména na straně uživatele.
10. Komentáře (docstring) všech funkcí aplikace, včetně Python typingu.
11. Stránka 404.
12. Úprava README.md v rámci repozitáře.

Uvedená posloupnost plánovaných funkcí nemusí odpovídat realitě samotné implementace.

# Licence
Aplikace je distribuována pod licencí [MIT](LICENSE.md).

# Pomož s vývojem
Pomoc s vývojem je vítána. Pokud se chcete do vývoje zapojit, kontaktujte nás na [josef.heidler@portabo.cz](mailto:josef.heidler@portabo.cz).