# Changelog

Všechny významné změny v projektu budou zdokumentovány v tomto souboru.

Formát je založen na [Keep a Changelog](https://keepachangelog.com/en/1.0.0/). Tento projekt dodržuje [Sémantické verzování](https://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2023-04-10

- Vypublikovaná první veřejná verze.
- Instance aplikace běží na adrese [kappka.portabo.cz](https://kappka.portabo.cz/).