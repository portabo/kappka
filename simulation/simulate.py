import sched
import time
import pandas as pd

from utils import influx_config, prepare_data, simulate, import_data

schedule = sched.scheduler(time.time, time.sleep)

config = influx_config()

influx_data = pd.read_csv("data/data.csv")
data = prepare_data(influx_data)

import_data(config, data["import"])
data["simulate"].apply(simulate, args=(schedule, config), axis=1)
schedule.run()
