import os
import json
from datetime import datetime
import pandas as pd
from dotenv import load_dotenv
import requests
import json
import sched
from influxdb_client import InfluxDBClient


def prepare_dataframe(dfs: list[pd.DataFrame]) -> pd.DataFrame:
    # Je nutné, aby dataframe, který je importován do Influxu měl jako index čas v datovém typu datetime.
    df = pd.concat(dfs)
    df["timestamp"] = pd.to_datetime(df["timestamp"], unit="s", origin="unix")
    df.set_index("timestamp", inplace=True)
    df.index.name = "datetime"

    return df


def prepare_data(data: pd.DataFrame) -> dict[pd.DataFrame]:
    ts_now = int(datetime.now().timestamp())
    watermeters = data.groupby("place")

    import_data = []
    simulate_data = []

    for wm in watermeters:
        data = wm[1]
        ts_origin = int(data.iloc[0]["timestamp"])
        time_diff = ts_now - ts_origin

        data["timestamp"] = data["timestamp"] + int(time_diff / 2)

        import_data.append(data.loc[data["timestamp"] < ts_now])
        simulate_data.append(data.loc[data["timestamp"] >= ts_now])

    import_data = prepare_dataframe(import_data)
    simulate_data = prepare_dataframe(simulate_data)

    return {"import": import_data, "simulate": simulate_data}


def parse_items(msg: str, places_to_convert: list[str]) -> str:
    # Tato funkce je na straně přijmu dat a není použita v tomto stacku.
    # places_to_convert = ["A1", "B1", "zsvojanova"]
    output = []

    msg = json.loads(msg)
    if "|" in msg["misto"]:
        places = msg["misto"].split("|")
        values = [msg["stav"], msg["stav1"]]
        for i, x in enumerate(places):
            place = places[i].strip()
            value = int(values[i].strip())
            value = unit_conversion(value, places_to_convert)
            output.append({"misto": place, "stav": value})
    else:
        place = msg["misto"].strip()
        value = int(msg["stav"].strip())
        value = unit_conversion(value, places_to_convert)
        output.append({"misto": place, "stav": value})

    return json.dumps(output)


def unit_conversion(places_to_convert: list[str], place: str, value: int) -> int:
    if place in places_to_convert:
        return value * 10
    else:
        return value


def influx_config() -> dict[str]:
    load_dotenv()

    return {
        "DEBUG": True if os.getenv("DEBUG") == "true" else False,
        "INFLUXDB_HOST": os.getenv("DOCKER_INFLUXDB_INIT_HOST"),
        "INFLUXDB_TOKEN": os.getenv("DOCKER_INFLUXDB_INIT_ADMIN_TOKEN"),
        "INFLUXDB_ORG": os.getenv("DOCKER_INFLUXDB_INIT_ORG"),
        "INFLUXDB_PORT": os.getenv("DOCKER_INFLUXDB_INIT_PORT"),
        "INFLUXDB_BUCKET": os.getenv("DOCKER_INFLUXDB_INIT_BUCKET"),
        "INFLUXDB_MEASUREMENT_NAME": os.getenv("INFLUXDB_MEASUREMENT_NAME"),
        "TELEGRAF_HOST": os.getenv("TELEGRAF_HOST"),
        "TELEGRAF_PORT": os.getenv("TELEGRAF_PORT"),
    }


def send_message(msg: dict[str], config: dict[str]) -> None:
    response = requests.post(
        url=f"http://{config['TELEGRAF_HOST']}:{config['TELEGRAF_PORT']}/vodomery",
        data=json.dumps(msg),
    )

    print(
        f"HTTP {response.status_code} | {msg}"
    )  # TODO: Spíše formou logování, tak aby bylo vidět v dockeru.


def simulate(row: tuple, schedule: sched.scheduler, config: dict[str]) -> None:
    dt = row.name
    place = row[0]
    value = row[1]
    msg = [{"misto": place, "stav": value}]
    schedule.enterabs(dt.timestamp(), 1, send_message, argument=(msg, config))


def import_data(config: dict[str], data: pd.DataFrame):
    data["host"] = "influx"
    data["source"] = "import"

    with InfluxDBClient(
        url=f"http://{config['INFLUXDB_HOST']}:{config['INFLUXDB_PORT']}",
        token=config["INFLUXDB_TOKEN"],
        org=config["INFLUXDB_ORG"],
        timeout=60000,
        debug=False,  # FIXME: Jakmile bude vše funkční, vrátit proměnnou z .env. (config["DEBUG"])
    ) as client:
        with client.write_api() as writer:
            status = writer.write(
                bucket=config["INFLUXDB_BUCKET"],
                record=data,
                data_frame_measurement_name=config["INFLUXDB_MEASUREMENT_NAME"],
                data_frame_tag_columns=["place", "host", "source"],
                write_precision="s",
            )

    if status == None:
        print(
            f"Importováno {len(data)} záznamů."
        )  # TODO: Spíše formou logování, tak aby bylo vidět v dockeru.
